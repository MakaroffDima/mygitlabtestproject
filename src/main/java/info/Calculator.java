package info;

public class Calculator {
    //нам не принципиально, какую логику будут реализовать наши методы
// данный метод возвращает сумму двух чисел
    public int sum(int x, int y){
        return x + y;
    }
    //данный метод возвращает произведение двух чисел
    public int multiplication(int x, int y){
        return x * y;
    }
}
